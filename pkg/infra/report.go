package infra

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

type Report struct {
	unique,
	newUnique,
	newDuplicate *uint32
	mutex sync.Mutex
}

func NewReport() *Report {
	return &Report{
		unique:       new(uint32),
		newUnique:    new(uint32),
		newDuplicate: new(uint32),
	}
}

func (r *Report) AddUnique() {
	atomic.AddUint32(r.newUnique, 1)
	atomic.AddUint32(r.unique, 1)
}

func (r *Report) AddDuplicate() {
	atomic.AddUint32(r.newDuplicate, 1)
}

func (r *Report) PrintReport() {
	for {
		time.Sleep(10 * time.Second)
		fmt.Printf("Received %d unique numbers, %d duplicates. Unique total: %d\n", *r.newUnique, *r.newDuplicate, *r.unique)
		r.resetNew()
	}
}

func (r *Report) resetNew() {
	r.mutex.Lock()
	r.newUnique = new(uint32)
	r.newDuplicate = new(uint32)
	r.mutex.Unlock()
}
