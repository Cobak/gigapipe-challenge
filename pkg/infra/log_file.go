package infra

import (
	"io"
	"os"
	"strconv"
	"sync"
)

type LogFile struct {
	out   io.Writer
	mutex sync.Mutex
}

func NewLogFile() LogFile {
	f, err := os.OpenFile("numbers.log", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		panic(err)
	}

	return LogFile{
		out:   f,
		mutex: sync.Mutex{},
	}
}

func (l *LogFile) Write(n int) error {
	_, err := l.out.Write([]byte(strconv.Itoa(n) + "\n"))
	return err
}
