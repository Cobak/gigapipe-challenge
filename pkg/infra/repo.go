package infra

import "sort"

type SliceRepository struct {
	digits sort.IntSlice
}

func NewSliceRepo() SliceRepository {
	return SliceRepository{
		digits: sort.IntSlice{},
	}
}

// Save search inside slice and return false if the integer already exists
func (r *SliceRepository) Save(n int) bool {
	in := r.digits.Search(n)
	if in < len(r.digits) && r.digits[in] == n {
		return false
	}

	r.digits = insert(r.digits, in, n)

	return true
}

func insert(a []int, index int, value int) []int {
	if len(a) == index { // nil or empty slice or after last element
		return append(a, value)
	}
	a = append(a[:index+1], a[index:]...) // index < len(a)
	a[index] = value
	return a
}