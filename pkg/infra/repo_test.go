package infra

import (
	"math/rand"
	"testing"
	"time"
)

func BenchmarkSliceSave(b *testing.B) {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	sRepo := NewSliceRepo()

	for n := 0; n < b.N; n++ {
		randN := r1.Intn(10000)
		sRepo.Save(randN)
	}
}

func BenchmarkMapSave(b *testing.B) {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	mRepo := NewMapRepo()
	for n := 0; n < b.N; n++ {
		randN := r1.Intn(10000)
		mRepo.Save(randN)
	}
}
