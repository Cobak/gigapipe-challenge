package infra

import (
	"bytes"
	"os"
	"testing"
)

func TestLogFile_Write(t *testing.T) {
	t.Run("must be created anew and/or cleared when the\n   application starts", func(t *testing.T) {
		for i := 0; i < 2; i++ {
			l := NewLogFile()
			err := l.Write(123456789)
			if err != nil {
				t.Errorf("Write() error = %v", err)
			}

			f, err := os.Open("numbers.log")
			if err != nil {
				t.Errorf("could not open number.log file: %v", err)
			}

			number := []byte{}

			_, err = f.Read(number)
			if err != nil {
				t.Errorf("could not read number.log file: %v", err)
			}

			if bytes.Equal(number, []byte("123456789")) {
				t.Errorf("data readed is not valid: got %s, expected %s", string(number), []byte("123456789"))
			}
		}
		_ = os.Remove("numbers.log")
	})
}
