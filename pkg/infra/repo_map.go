package infra

import "sync"

type MapRepository struct {
	digits map[int]bool
	mutex  sync.Mutex
}

func NewMapRepo() MapRepository {
	return MapRepository{
		digits: make(map[int]bool),
		mutex:  sync.Mutex{},
	}
}

// Save search inside slice and return false if the integer does not exist
func (r *MapRepository) Save(n int) bool {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	if r.digits[n] {
		return true
	}

	r.digits[n] = true

	return false
}
