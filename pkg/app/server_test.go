package app

import (
	"io/ioutil"
	"log"
	"net"
	"os"
	"sync"
	"testing"
)

func TestServe(t *testing.T) {
	t.Run("should limit the number of clients that connect and send data to the app", func(t *testing.T) {
		ln, err := net.Listen("tcp", "127.0.0.1:4000")
		if err != nil {
			panic(err)
		}
		defer ln.Close()

		maxCon := 2

		go Serve(ln, maxCon)

		wg := sync.WaitGroup{}
		for i := 0; i < 6; i++ {
			wg.Add(1)
			client := Client{}
			defer client.Close()

			err = client.Connect()
			if err != nil {
				log.Printf("connection failed")
				continue
			}

			for j := 0; j < 1; j++ {
				client.Send()
			}
			wg.Done()
		}
		wg.Wait()

		f, err := os.Open("numbers.log")
		if err != nil {
			t.Errorf("could not open numbers.log file: %v", err)
		}

		data,err := ioutil.ReadAll(f)
		if err != nil {
			t.Errorf("could not read data: %v", err)
		}
		if len(data) > maxCon * 10 {
			t.Errorf("data should not be above the number of connections plus bytes sended to the server")
		}
	})
}
