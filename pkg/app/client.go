package app

import (
	"context"
	"fmt"
	"math/rand"
	"net"
	"time"
)

type Client struct {
	conn net.Conn
}

func (c *Client) Connect() error {
	dialCtx, cancelDial := context.WithCancel(context.Background())
	defer cancelDial()
	dialer := &net.Dialer{}

	conn, err := dialer.DialContext(dialCtx, "tcp", "127.0.0.1:4000")
	if err != nil {
		return fmt.Errorf("could not connect to server: %v", err)
	}

	c.conn = conn

	return nil
}

func (c *Client) Close() {
	err := c.conn.Close()
	if err != nil {
		panic(err)
	}
}

func (c *Client) Send() error {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)

	randN := fmt.Sprintf("%09d", r1.Intn(999999999))

	n, err := fmt.Fprint(c.conn, randN+"\n")
	if n == 0 {
		return fmt.Errorf("client closed")
	}
	if err != nil {
		c.Close()
		panic(err)
	}
	return nil
}


func (c *Client) SendTerminate() error {
	_, err := fmt.Fprint(c.conn, "terminate\n")
	if err != nil {
		c.Close()
		panic(err)
	}
	return nil
}