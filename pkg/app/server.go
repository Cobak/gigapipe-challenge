package app

import (
	"net"
	"os"

	"golang.org/x/net/netutil"

	"gitlab.com/cobak/gigapipe-challenge/pkg/infra"
)

type LogFile interface {
	Write(n int) error
}

func Serve(ln net.Listener, maxCon int) {
	// di
	repo := infra.NewMapRepo()
	logFile := infra.NewLogFile()
	rep := infra.NewReport()
	go rep.PrintReport()

	limitLn := netutil.LimitListener(ln, maxCon)

	terminate := make(chan bool)
	go func() {
		<-terminate
		_ = ln.Close()
		os.Exit(0)
	}()

	for {
		conn, err := limitLn.Accept()
		if err != nil {
			panic(err)
		}
		go HandleConnection(conn, rep, &repo, &logFile, terminate)
	}
}
