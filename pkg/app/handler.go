package app

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"strconv"

	"gitlab.com/cobak/gigapipe-challenge/pkg/infra"
)

const bytesToRead = 10

func HandleConnection(conn net.Conn, rep *infra.Report, repo *infra.MapRepository, log LogFile, terminate chan bool) {
	for {
		data := make([]byte, bytesToRead)
		n, err := conn.Read(data)
		if err != nil {
			if err != io.EOF {
				panic(err)
			}
			break
		}

		if bytes.Equal(data, []byte("terminate\n")) {
			terminate <- true
		}

		digit, err := strconv.Atoi(string(data[:n-1]))
		if err != nil {
			fmt.Print(err.Error())
			terminate <- true
		}

		if repo.Save(digit) {
			rep.AddDuplicate()
			continue
		}

		err = log.Write(digit)
		if err != nil {
			panic(err)
		}

		rep.AddUnique()
	}
}
