# gigapipe-challenge

### Solution

The project has two main.go files (client and server) that sets up the environment of the challenge.

1. Server: 
   1. Max connections defined by the environment variable MAX_CONNECTION
   2. Log file writer defined in `infra/log_file.go` and injected to the server.
   3. Report service defined in `infra/report.go` and injected to the server
   4. Repository to handle deduplication and counting defined in `infra/repo_map.go`.
      1. There are two possible implementations of the repository but for performance reasons I stand for the map implementation
      2. There's a benchmark test to compare both implemantations
2. Client.
   1. A simple service with two flags (**clients** and **digits**) that connects and send formatted data

### Lib / tools

- netutil to limit tcp connections
- atomic counters and mutex to handle collisions
- sort and IntSlice in the first attempt of handle data in the repository

### Setup & launch

Open two consoles and launch server and client. Use the makefile to help with both:
```bash
    make server_up
    make client_send
```

### Tests & Benchmark

Different implementations of the deduplication process:

- cpu: Intel(R) Core(TM) i7-1068NG7 CPU @ 2.30GHz
- BenchmarkSliceSave-8   	 9779682	       115.1 ns/op
- BenchmarkMapSave-8     	30093124	        39.41 ns/op

```bash
make test
```

### chanllenge description

[challenge.md](etc/doc/challenge_description.md)