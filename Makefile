.PHONY: server_up client_send test

server_up:
	MAX_CONNECTIONS=4 go run cmd/server/main.go

client_send:
	go build -o client_send cmd/client/main.go
	./client_send -clients=5 -digits=100000000

test:
	go test -count=1 ./...