package main

import (
	"net"
	"os"
	"strconv"

	"gitlab.com/cobak/gigapipe-challenge/pkg/app"
)

func main() {
	maxCon, err := strconv.Atoi(os.Getenv("MAX_CONNECTIONS"))
	if err != nil {
		panic("MAX_CONNECTIONS must be set and be a valid integer" + err.Error())
	}

	ln, err := net.Listen("tcp", "127.0.0.1:4000")
	if err != nil {
		panic(err)
	}

	app.Serve(ln, maxCon)
}
