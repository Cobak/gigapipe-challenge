package main

import (
	"flag"
	"log"
	"sync"

	"gitlab.com/cobak/gigapipe-challenge/pkg/app"
)

func main() {
	nCli, nDig := getFlags()

	wg := sync.WaitGroup{}

	for i := 0; i < nCli; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			client := app.Client{}
			defer client.Close()

			err := client.Connect()
			if err != nil {
				log.Print(err.Error())
				return
			}

			log.Printf("connection: %v", client)

			for j := 0; j < nDig; j++ {
				err := client.Send()
				if err != nil {
					return
				}
			}
		}()
	}

	wg.Wait()
}

func getFlags() (nClients int, nDigits int) {
	clients := flag.Int("clients", 1, "max number of clients trying to connect. default 1")
	digits := flag.Int("digits", 1, "max number of digits passed to the server by every client. default 1")
	flag.Parse()
	nClients = *clients
	nDigits = *digits

	return
}
